import UIKit
import Foundation
import Liveness


@objc(LivenessDetect)
class LivenessDetect: UIViewController ,  LivenessOnLoad, LivenessTestDelegate {
     var liveness : LivenessTest? = nil
    override func loadView() {
        super.loadView()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
  
  
  @objc
  func startLiveness(){

    let config: LivenessTestConfig = LivenessTestConfig(provider: Provider.FACETEC, baseUrl: "https://d1.cynopsis.co/service", region: "ap-southeast-1",domain:"cynopsis", clientId:"", clientSecret: "" )
    self.liveness = LivenessTest(fromVC: self, config: config, livenessOnLoad: self)
      // Theming
      LivenessCutomization.setBackgroundColor(colorString: "#FFFFFF")
      LivenessCutomization.setThemeColor(colorString: "#FF0000")
      LivenessCutomization.setButtonTextColor(colorString: "#FFFFFF")
      LivenessCutomization.setTextColor(colorString: "#000000")

      //    let viewController = UIApplication.shared.keyWindow?.rootViewController
      //    viewController!.present(self, animated: true, completion: nil)

     
        
    
    
      
  }

  func getRootVC() -> UIViewController {
      var root = UIApplication.shared.keyWindow?.rootViewController

      while root?.presentedViewController != nil {
          root = root?.presentedViewController
      }

      return root!
  }
  // Callback functions
  func succeed(message: String) {
    // do something
    print(message)
    var postBody = [AnyHashable:Any]()
    postBody["message"] = message
  //      Start Liveness
    self.liveness?.startLiveness(livenessTestDelegate: self)
    EventEmitter.sharedInstance.dispatch(name: "onReady", body: postBody)
  }
  func failed(message: String) {
        // do something
    print(message)
    
    var postBody = [AnyHashable:Any]()
    postBody["message"] = message
  
    EventEmitter.sharedInstance.dispatch(name: "onReady", body: postBody)
  }
  func onResponse(response: LivenessTestResults) {
      print(response)
      var postBody = [AnyHashable:Any]()
    postBody["message"] = response.message
    postBody["status"] = response.status
    postBody["faceId"] = response.faceId
    postBody["sessionId"] = response.sessionId
    postBody["selfieImageBase64"] = response.selfieImageBase64

    
      EventEmitter.sharedInstance.dispatch(name: "onResponse", body: postBody)
    
    
      switch response.status {
          case .ERROR:
              // do something
            print("error")
            break
          case .FAILED:
              // do something
            print("faild")
            break
          case .PASSED:
            print("PASSED")
              // do something
            break
          
      }
  }
}

