//
//  Counter.m
//  kikitrade
//
//  Created by vincent on 2020/7/18.
//  Copyright © 2020 Facebook. All rights reserved.
//

// Counter.m
#import "React/RCTBridgeModule.h"
#import <React/RCTEventEmitter.h>
#import <Foundation/Foundation.h>

@interface RCT_EXTERN_MODULE(LivenessDetect, NSObject)

RCT_EXTERN_METHOD(startLiveness)


@end
