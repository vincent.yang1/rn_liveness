package com.rnliveness;


import android.util.Log;
import android.content.Context;

import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.modules.core.DeviceEventManagerModule;
import com.facebook.react.uimanager.IllegalViewOperationException;

import java.util.HashMap;
import java.util.Map;

import com.liveness.LivenessCustomization;
import com.liveness.LivenessOnLoad;
import com.liveness.LivenessTest;
import com.liveness.LivenessTestConfig;
import com.liveness.LivenessTestDelegate;
import com.liveness.LivenessTestResults;
import com.liveness.Provider;

import android.app.Activity;


import javax.annotation.Nullable;

public class LivenessDetectModule extends ReactContextBaseJavaModule {

    private int count = 0;
    private LivenessDetectModule  self = this;
    private LivenessTest liveness;
    public LivenessDetectModule(ReactApplicationContext reactApplicationContext) {
        super(reactApplicationContext);
    }

    @Override
    public String getName() {
        return "LivenessDetect";
    }

    @Override
    public Map<String, Object> getConstants() {
        final Map<String, Object> constants = new HashMap<>();
        constants.put("initialCount", 0);
        return constants;
    }

    @ReactMethod
    public void startLiveness() {
//        callback.invoke(count);
        // change to LivenessTestConfig(Provider provider, String serviceBaseurl, String accessToken)
        LivenessTestConfig config = new LivenessTestConfig(Provider.FACETEC, "https://d1.cynopsis.co/service", "" );

        LivenessOnLoad livenessOnLoad = new LivenessOnLoad() {
            @Override
            public void succeed(String message) {
                // do something
                WritableMap params = Arguments.createMap();
                params.putString("message", message);
                sendEvent(self.getReactApplicationContext(), "onReady", params);
                // Start Liveness

                // Callback functions
                LivenessTestDelegate livenessTestDelegate = new LivenessTestDelegate() {
                    @Override
                    public void onResponse(LivenessTestResults response) {
                        WritableMap params = Arguments.createMap();
                        params.putString("faceId", response.getFaceId());
                        params.putString("message", response.getMessage());
                        params.putString("sessionId", response.getSessionId());
                        params.putString("selfieImageBase64", response.getSelfieImageBase64());
                        params.putString("status", response.getStatus().toString());

                        sendEvent(self.getReactApplicationContext(), "onResponse", params);



                        switch (response.getStatus()) {
                            case ERROR: {
                                // do something
                                Log.e("","ERROR");

                                break;
                            }
                            case FAILED: {
                                // do something
                                Log.e("","FAILED");

                                break;
                            }
                            case PASSED: {
                                // do something
                                Log.e("","PASSED");

                                break;
                            }
                        }
                    }
                };

                self.liveness.startLiveness(livenessTestDelegate);
            }
            @Override
            public void failed(String message) {
                // do something
                Log.d("liveness failed",message);
                WritableMap params = Arguments.createMap();
                params.putString("message", message);
                sendEvent(self.getReactApplicationContext(), "onReady", params);
            }
        };
        final Activity activity = getCurrentActivity();
        this.liveness = new LivenessTest(activity, config, livenessOnLoad);
        //Theming
        LivenessCustomization.setThemeColor("#68b5de");
        LivenessCustomization.setBackgroundColor("#FFFFFF");
        LivenessCustomization.setTextColor("#000000");
        LivenessCustomization.setButtonTextColor("#FFFFFF");






    }


    private void sendEvent(
            ReactContext reactContext,
            String eventName,
            @Nullable WritableMap params) {
        reactContext.getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class)
                .emit(eventName, params);
    }
}