/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { Component } from "react";
import {
  SafeAreaView,
  NativeModules,
  TouchableOpacity,
  PermissionsAndroid,
  NativeEventEmitter,
  Platform,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';


class App extends Component {

  constructor(props) {
      super(props);
      this.state = {
      };
  }

  componentDidMount() {
    let that = this
    if(Platform.OS == "android") {
      this.requestPermission();
    }
    
    const counterEvents = new NativeEventEmitter(NativeModules.ReactNativeEventEmitter);

    counterEvents.addListener('onReady', (message) => {
      alert(JSON.stringify(message))
      console.log(message)
    });
    counterEvents.addListener('onResponse', (message) => {
      alert(JSON.stringify(message))
      console.log(message)
      that.setState({
        livenessing:false
      })
    });

  }


  requestPermission =  () => {
    try {
      PermissionsAndroid.requestMultiple(
        [PermissionsAndroid.PERMISSIONS.CAMERA, 
        PermissionsAndroid.PERMISSIONS.READ_CONTACTS,
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION,
        PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
        PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE]
        ).then((result) => {
          if (result['android.permission.ACCESS_COARSE_LOCATION']
          && result['android.permission.CAMERA']
          && result['android.permission.READ_CONTACTS']
          && result['android.permission.ACCESS_FINE_LOCATION']
          && result['android.permission.READ_EXTERNAL_STORAGE']
          && result['android.permission.WRITE_EXTERNAL_STORAGE'] === 'granted') {

          } else if (result['android.permission.ACCESS_COARSE_LOCATION']
          || result['android.permission.CAMERA']
          || result['android.permission.READ_CONTACTS']
          || result['android.permission.ACCESS_FINE_LOCATION']
          || result['android.permission.READ_EXTERNAL_STORAGE']
          || result['android.permission.WRITE_EXTERNAL_STORAGE'] === 'never_ask_again') {
          }
        });
    } catch (err) {
      console.warn(err);
    }
  };

  start = () => {
      this.setState({
        livenessing:true
      })
     NativeModules.LivenessDetect.startLiveness()
  }



  render() {
    let disabled = !!this.state.livenessing
    return (
      <>
        <SafeAreaView>
          <ScrollView
            contentInsetAdjustmentBehavior="automatic"
            style={styles.scrollView}>
            <Header />

            <View style={[styles.body,{alignItems:"center",justifyContent:"center"}]}>
              <TouchableOpacity disabled={disabled} onPress={this.start} style={{backgroundColor:"#333",width:120,height:40,alignItems:"center", borderRadius:6,justifyContent:"center", opacity:disabled ? 0.4 : 1}}>
                <Text style={{color:"white"}}>Start to liveness</Text>
              </TouchableOpacity>
            </View>
          </ScrollView>
        </SafeAreaView>
      </>
    );
  }

};

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: Colors.lighter,

  },
  engine: {
    position: 'absolute',
    right: 0,
  },
  body: {
    backgroundColor: Colors.white,
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
    color: Colors.black,
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
    color: Colors.dark,
  },
  highlight: {
    fontWeight: '700',
  },
  footer: {
    color: Colors.dark,
    fontSize: 12,
    fontWeight: '600',
    padding: 4,
    paddingRight: 12,
    textAlign: 'right',
  },
});

export default App;
